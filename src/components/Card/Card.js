import React, { Component } from 'react'
import './Card.css';

class Card extends Component {

 constructor(props) {
   super(props);
 }

onClick (event) {
  console.log(event.target.value)
}


 render() {
   return (
   <div>
   {this.props.listItems.map(
            (item,index) =>
                (
                 <div className="card mb-3" key={item.id}>
                  <div className="row no-gutters">
                    <div className="col-md-4">
                      <img src="https://via.placeholder.com/150X150.png" className="img-thumbnail" alt="..."></img>
                    </div>
                    <div className="col-md-8">
                      <div className="card-body">
                        <h5 className="card-title">{item.name}</h5>
                        <p className="card-text"> el precio es {item.price}</p>
                        <p className="card-text"> el precio es {item.quantity}</p>
                        <button value={item.id} type="button" onClick={event => this.onClick(event)}>ver mas</button>
                      </div>
                    </div>
                  </div>
                </div>
               )
            )
    }
   </div>
   );
 }
}

export default Card;
