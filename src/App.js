import React, { Component } from 'react'

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'

import './App.css';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';


class App extends Component {

 constructor(props) {
   super(props);
   this.state = {
     items: [],
     hideItems: false,
     links: ["a", "b"],
   };
 }

componentDidMount () {
  const token = localStorage.getItem("token");

  if (token) {
    fetch('http://localhost:1337/careers', {
      method: 'get',
      body: null,
      headers: {
        'Authorization': `Bearer ${token}`,
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    })
    .then(response => response.json())
    .then(data => {
       console.log("careers", data)
    });
  }

}

logout(){
  localStorage.removeItem('token')
  localStorage.removeItem('user')
}


login (event) {
  fetch('http://localhost:1337/auth/local', {
    method: 'post',
    body: JSON.stringify(
      {
        identifier: 'admin@tecnica7.edu.ar',
        password: 'password1',
      }
    ),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
  .then(response => response.json())
  .then(data => {
      localStorage.setItem("token", data.jwt);
      localStorage.setItem("user", JSON.stringify(data.user));
  });
}

 render() {
   return (

   <Container fluid="true">
     <Header color="green" texto="MI APP"></Header>
    <Row>
      <Col md={4}>
        <Button
          variant="primary"
          onClick={this.login}
        >
        loguear
        </Button>
        <Button
          variant="secondary"
          onClick={this.logout}
        >
        logout
        </Button>
      </Col>
    </Row>
    <Footer></Footer>
  </Container>

   );
 }
}

export default App;
